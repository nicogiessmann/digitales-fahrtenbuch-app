Maintainer: Nico Gießmann  
Kontakt: <nigi4901@th-wildau.de>  
Zuletzt bearbeitet: 02.05.20  


# Webapplikation für das Digitale Fahrtenbuch und Fuhrparkmanagement

Mithilfe dieses Repositories lässt sich mit Python die Webapplikation für das
Digitales Fuhrparkmanagement und Fahrtenbuch einrichten.

## Installationsanleitung

- Klonen des Repositories

    ```
    $ git clone git@gitlab.com:nicogiessmann/digitales-fahrtenbuch-app.git
    ```

- Erstelle und Aktivieren einer Python Virtual Environment

    ```
    $ python3 -m venv .env
    $ source .env/bin/active # Nur unter Linux
    ```

- Starten der Django-Webapplikation

    ```
    $ cd project
    $ python3 manage.py runserver
    ```

- Der Webserver läuft nun auf 127.0.0.1 (localhost) und hört auf Port 8000

Bei Fragen wenden sie sich gerne an den Maintainer.
