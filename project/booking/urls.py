from django.urls import path

from . import views
from .apps import BookingConfig

app_name = BookingConfig.name

urlpatterns = [
    path("", views.index, name="index"),
    path("booking", views.booking, name="booking"),
    path("book_car", views.book_car, name="book_car"),
    path("success", views.success, name="success"),
]
