import db_module
import djangoHelper
from db_module import better_get
from django.http import HttpRequest, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from djangoHelper import check_query, get_date, get_time, get_datetime, url_with_parameters, get_error_dict
from typing import Union

from .apps import BookingConfig


def index(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefer den View für die Startseite der Buchung.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, BookingConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Buchungen zu tätigen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    return render(request, "booking/index.html", {"booking_active": "active"})


def booking(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für das Buchungsformular.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, BookingConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Buchungen zu tätigen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    instition_id = db_module.get_user_infos(session_check)
    if not check_query(instition_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Institution.",
                                                                       reverse("booking:index"))))
    instition_id = instition_id[4]

    cars = db_module.get_cars_of_institution(instition_id)
    if not check_query(cars):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen von Fahrzeugdaten.",
                                                                       reverse("booking:index"))))

    context = {"cars": cars, "booking_active": "active"}

    return render(request, "booking/booking.html", context)


def book_car(request: HttpRequest) -> HttpResponseRedirect:
    """
    Die Funktion bucht ein Fahrzeug.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, BookingConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Buchungen zu tätigen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    start_date = get_date(better_get(request, "start_date"))
    start_time = get_time(better_get(request, "start_time"))
    start_datetime = get_datetime(start_date, start_time)
    end_date = get_date(better_get(request, "end_date"))
    end_time = get_time(better_get(request, "end_time"))
    end_datetime = get_datetime(end_date, end_time)

    car_id = better_get(request, "car")

    booking_id = db_module.insert_booking(start_datetime, end_datetime, car_id, session_check)
    if not check_query(booking_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Hinzufügen einer neuen Buchung.",
                                                                       reverse("booking:booking"))))

    check = db_module.set_booking_state(booking_id, db_module.BOOKING_ABGEHOLT)
    if not check_query(check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Ändern des Buchungsstatus.",
                                                                       reverse("booking:booking"))))

    return HttpResponseRedirect(reverse("booking:success"))


def success(request) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für die Bestätigungsseite der Buchung
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, BookingConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Buchungen zu tätigen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    return render(request, "booking/success.html", {"booking_active": "active"})
