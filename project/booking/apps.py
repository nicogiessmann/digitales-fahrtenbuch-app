import db_module
from django.apps import AppConfig


class BookingConfig(AppConfig):
    name = 'booking'
    verbose_name = "Fahrzeug buchen"
    roles = (db_module.ROLE_FAHRER,)
    icon = "fas fa-calendar-alt"
