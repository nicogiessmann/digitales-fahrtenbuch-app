from django.urls import path

from . import views
from .apps import PersonalConfig

app_name = PersonalConfig.name

urlpatterns = [
    path("", views.index, name="index"),
    path("change_pass", views.change_pass, name="change_pass"),
]
