import db_module
import djangoHelper
from db_module import better_get
from django.http import HttpRequest, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from djangoHelper import url_with_parameters, get_error_dict, check_query
from typing import Union


def index(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für die Startseite der Profilseite und listet Nutzerdaten auf.
    """
    # Session checks
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    check_or_user_infos = db_module.get_user_infos(session_check)
    if not check_query(check_or_user_infos):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Nutzerinformationen.",
                                                                       reverse("frontpage:login"))))

    # User infos successfully transmitted, add to context
    context = {
        "first_name": check_or_user_infos[0],
        "last_name": check_or_user_infos[1],
        "email": check_or_user_infos[2],
        "institution": check_or_user_infos[3],
    }

    return render(request, "personal/index.html", context)


def change_pass(request: HttpRequest) -> HttpResponseRedirect:
    """
    Die Funktion verändert das Passwort des Nutzers.
    """
    # Session checks
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    old_pass = better_get(request, "old_pass")
    new_pass = better_get(request, "new_pass")

    if old_pass != new_pass:
        check = db_module.change_password(session_check, old_pass, new_pass)
        if not check_query(check):
            return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                            get_error_dict("500", "Internal Server Error",
                                                                           "Fehler beim Ändern des Passwortes.",
                                                                           reverse("personal:index"))))
        return HttpResponseRedirect(reverse("personal:index"))
    return HttpResponseRedirect(url_with_parameters("dashboard:error", get_error_dict("900", "ValueError",
                                                                                      "Die Passwörter stimmen nicht überein.",
                                                                                      reverse("personal:index"))))
