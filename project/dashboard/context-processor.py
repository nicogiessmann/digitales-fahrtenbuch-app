"""
Context processor for dashboards
"""
import db_module
import djangoHelper
from django.http import HttpRequest
from djangoHelper import check_query


def nav_items(request: HttpRequest) -> dict:
    """
    Die Funktion stellt auf allen Templates die für den aktuell angemeldeten Nutzer verfügbaren Apps zur Verfügung.

    :param request: Django http request
    :return: dict mit Apps
    """
    permitted_apps = djangoHelper.get_permitted_apps(request)
    check_or_user_id = db_module.get_user_id_from_session(djangoHelper.get_session_cookie(request))
    if not check_query(check_or_user_id): return dict()

    user_infos = db_module.get_user_infos(check_or_user_id)
    if not check_query(user_infos): return dict()

    return {
        "permitted_apps": permitted_apps,
        "user_infos": user_infos[0] + " " + user_infos[1],
    }
