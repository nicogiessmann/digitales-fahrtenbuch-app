import djangoHelper
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from djangoHelper import url_with_parameters, get_error_dict


def index(request: HttpRequest):
    """
    Die Funktion liefert den View für die Startseite.
    """
    # Session checks
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    context = {"dashboard_active": "active"}

    return render(request, "dashboard/index.html", context)


def logout(request: HttpRequest):
    """
    Die Funktion loggt einen Nutzer aus.
    """
    response = HttpResponseRedirect(reverse("frontpage:login"))
    djangoHelper.delete_session(request, response)
    return response


def error(request: HttpRequest):
    """
    Die Funktion liefert den View für die Fehlerseite.
    """
    # No session check on error
    context = {
        "error_code": request.GET.get("error_code"),
        "error_title": request.GET.get("error_title"),
        "error_msg": request.GET.get("error_msg"),
        "error_link": request.GET.get("error_link")
    }

    return render(request, "dashboard/error.html", context)
