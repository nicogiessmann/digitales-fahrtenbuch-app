from django.urls import path

from . import views
from .apps import FrontpageConfig

app_name = FrontpageConfig.name

urlpatterns = [
    path("", views.login, name="index"),
    path("login", views.login, name="login"),
    path("login_form", views.login_form, name="login_form"),
    path("signup", views.signup, name="signup"),
    path("signup_form", views.signup_form, name="signup_form"),
]
