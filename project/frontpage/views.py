import db_module
import djangoHelper
from db_module import better_get
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from djangoHelper import url_with_parameters, get_error_dict, check_query


# Create your views here.
def index(request: HttpRequest):
    return render(request, "frontpage/index.html")


def login(request: HttpRequest):
    # Session checks
    session_check = djangoHelper.is_active_session(request)
    if type(session_check) == int and session_check > 0:
        return HttpResponseRedirect(reverse("dashboard:index"))
    elif session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Sessiondaten",
                                                                       reverse("frontpage:index"))))

    return render(request, "frontpage/login.html")


def login_form(request: HttpRequest):
    # Session checks
    session_check = djangoHelper.is_active_session(request)
    if type(session_check) == int and session_check > 0:
        return HttpResponseRedirect(reverse("dashboard:index"))
    elif session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Sessiondaten",
                                                                       reverse("frontpage:index"))))

    email = better_get(request, "email")
    password = better_get(request, "password")
    if not email == None and not password == None:
        check_or_user_id = db_module.verify_user(email, password)
        if check_or_user_id == None:
            return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                            get_error_dict("900", "ValueError",
                                                                           "Fehler beim Abfragen der Zugangsdaten",
                                                                           reverse("frontpage:login"))))

        elif check_or_user_id == False:
            return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                            get_error_dict("500", "Internal Server Error",
                                                                           "Fehler beim Abfragen der Zugangsdaten",
                                                                           reverse("frontpage:login"))))
        else:
            # user verified
            check_or_session_obj = db_module.create_new_session(check_or_user_id)
            if check_or_session_obj == False:
                return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                                get_error_dict("500", "Internal Server Error",
                                                                               "Fehler beim Erstellen der Session",
                                                                               reverse("frontpage:login"))))
            else:
                response = HttpResponseRedirect(reverse("dashboard:index"))
                djangoHelper.set_session_cookie(response, check_or_session_obj[0], check_or_session_obj[1])
                return response

    return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                    get_error_dict("500", "Internal Server Error",
                                                                   "Fehler beim Abfragen der Zugangsdaten",
                                                                   reverse("frontpage:login"))))


def signup(request: HttpRequest):
    # Session checks
    session_check = djangoHelper.is_active_session(request)
    if type(session_check) == int and session_check > 0:
        return HttpResponseRedirect(reverse("dashboard:index"))
    elif session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Sessiondaten",
                                                                       reverse("frontpage:index"))))

    return render(request, "frontpage/signup.html")


def signup_form(request: HttpRequest):
    # Session checks
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if type(session_check) == int and session_check > 0:
        return HttpResponseRedirect(reverse("dashboard:index"))
    elif session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Sessiondaten",
                                                                       reverse("frontpage:index"))))

    # Getting all values from POST request
    title = better_get(request, "title")
    street = better_get(request, "street")
    house_number = better_get(request, "house_number")
    postalcode = better_get(request, "postalcode")
    town = better_get(request, "town")
    first_name = better_get(request, "first_name")
    last_name = better_get(request, "last_name")
    email = better_get(request, "email")
    password = better_get(request, "password")
    password_check = better_get(request, "password_check")

    # Checks
    if not password == password_check:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("900", "ValueError",
                                                                       "Passwörter stimmen nicht überein.",
                                                                       reverse("frontpage:signup"))))

    # Signup user
    location_id = db_module.insert_location(street, house_number, town, postalcode)
    if not check_query(location_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Erstellen eines Ortes.",
                                                                       reverse("frontpage:signup"))))
    institution_id = db_module.insert_institution(title, location_id)
    if not check_query(institution_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Erstellen einer Institution.",
                                                                       reverse("frontpage:signup"))))
    user_id = db_module.insert_user(first_name, last_name, email, password, False, institution_id)
    if not check_query(user_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Erstellen eines Nutzers",
                                                                       reverse("frontpage:signup"))))
    check = db_module.insert_user_role(user_id, db_module.ROLE_CONTROLLER)
    if not check_query(check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Zuordnen einer Rolle",
                                                                       reverse("frontpage:signup"))))

    # Create session
    check_or_session_obj = db_module.create_new_session(user_id)
    if check_or_session_obj == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Erstellen einen Session",
                                                                       reverse("frontpage:signup"))))
    else:
        response = HttpResponseRedirect(reverse("dashboard:index"))
        djangoHelper.set_session_cookie(response, check_or_session_obj[0], check_or_session_obj[1])
        return response
