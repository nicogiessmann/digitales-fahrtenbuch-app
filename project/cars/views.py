import db_module
import djangoHelper
from db_module import better_get
from django.http import HttpRequest, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.datetime_safe import datetime
from djangoHelper import url_with_parameters, get_error_dict, check_query
from typing import Union

from .apps import CarsConfig


def index(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für die Darstellung der Index Seite.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if not check_query(session_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, CarsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrzeuge hinzuzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    return render(request, "cars/index.html", {"cars_active": "active"})


def cars(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für das Formular zum Fahrzeuge hinzufügen.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if not check_query(session_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, CarsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrzeuge hinzuzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    context = {"car_types": db_module.get_car_types(), "cars_active": "active"}

    return render(request, "cars/cars.html", context)


def insert_car(request: HttpRequest) -> HttpResponseRedirect:
    """
    Die Funktion fügt ein neues Fahrzeug hinzu.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if not check_query(session_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, CarsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrzeuge hinzuzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    model = better_get(request, "model")
    manufacturer = better_get(request, "manufacturer")
    vin = better_get(request, "vin")
    seats = better_get(request, "seats")
    milage = better_get(request, "milage")
    first_registration = better_get(request, "first_registration")
    first_registration = datetime.strptime(first_registration, "%d.%m.%Y")
    type_id = better_get(request, "type_id")

    # Get institution id
    user_infos = db_module.get_user_infos(session_check)
    if not check_query(user_infos):
        if not check_query(user_infos):
            return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                            get_error_dict("500", "Internal Server Error",
                                                                           "Fehler beim Abfragen von Nutzerdaten.",
                                                                           reverse("cars:cars"))))

    institution_id = user_infos[4]

    check = db_module.insert_car(model, manufacturer, vin, seats, milage, first_registration, type_id, institution_id)
    if not check_query(check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Hinzufügen eines Fahrzeuges.",
                                                                       reverse("cars:cars"))))

    return HttpResponseRedirect(reverse("cars:success"))


def success(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für die Darstellung der Success Seite.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if not check_query(session_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, CarsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrzeuge hinzuzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    return render(request, "cars/success.html", {"cars_active": "active"})
