import db_module
from django.apps import AppConfig


class CarsConfig(AppConfig):
    name = 'cars'
    verbose_name = "Fahrzeug hinzufügen"
    roles = (db_module.ROLE_CONTROLLER,)
    icon = "fas fa-car"
