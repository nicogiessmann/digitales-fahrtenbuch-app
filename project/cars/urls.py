from django.urls import path

from . import views
from .apps import CarsConfig

app_name = CarsConfig.name

urlpatterns = [
    path("", views.index, name="index"),
    path("cars", views.cars, name="cars"),
    path("insert_car", views.insert_car, name="insert_car"),
    path("success", views.success, name="success"),
]
