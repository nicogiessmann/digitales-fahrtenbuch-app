import db_module
import djangoHelper
from db_module import better_get
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from djangoHelper import url_with_parameters, get_error_dict, check_query
from typing import Union

from .apps import UsersConfig

_USER_DEFAULT_PASS = "passwort"


def index(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für die Darstellung der Index Seite.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if not check_query(session_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, UsersConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Nutzer hinzuzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    return render(request, "users/index.html", {"users_active": "active"})


def users(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert einen View für das Formular zum Hinzufügen eines neuen Nutzers.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if not check_query(session_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, UsersConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Nutzer hinzuzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    return render(request, "users/users.html", {"users_active": "active"})


def insert_user(request) -> HttpResponseRedirect:
    """
    Die Funktion fügt einen neuen Nutzer hinzu.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if not check_query(session_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, UsersConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Nutzer hinzuzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    firstname = better_get(request, "first_name")
    lastname = better_get(request, "last_name")
    email = better_get(request, "email")
    role_controller = better_get(request, "role_controller")
    role_fahrer = better_get(request, "role_fahrer")

    # Hole ID der Institution
    institutionid = db_module.get_user_infos(session_check)
    if not check_query(institutionid):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Institution.",
                                                                       reverse("users:users"))))
    institutionid = institutionid[4]

    check_or_user_id = db_module.insert_user(firstname, lastname, email, _USER_DEFAULT_PASS, None, institutionid)
    if not check_query(check_or_user_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Hinzufügen des Nutzers.",
                                                                       reverse("users:users"))))

    if role_controller is not None:
        check = db_module.insert_user_role(check_or_user_id, db_module.ROLE_CONTROLLER)
        if not check_query(check):
            return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                            get_error_dict("500", "Internal Server Error",
                                                                           "Fehler beim Zuordnen einer Rolle",
                                                                           reverse("users:users"))))

    if role_fahrer is not None:
        check = db_module.insert_user_role(check_or_user_id, db_module.ROLE_FAHRER)
        if not check_query(check):
            return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                            get_error_dict("500", "Internal Server Error",
                                                                           "Fehler beim Zuordnen einer Rolle",
                                                                           reverse("users:users"))))

    return HttpResponseRedirect(reverse("users:success"))


def success(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für die Darstellung der Success Seite.
    """
    # Session check
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if not check_query(session_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, UsersConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Nutzer hinzuzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    return render(request, "users/success.html", {"users_active": "active"})
