import db_module
from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = "Nutzer hinzufügen"
    roles = (db_module.ROLE_CONTROLLER,)
    icon = "fas fa-users"
