from django.urls import path

from . import views
from .apps import UsersConfig

app_name = UsersConfig.name

urlpatterns = [
    path("", views.index, name="index"),
    path("users", views.users, name="users"),
    path("insert_user", views.insert_user, name="insert_user"),
    path("success", views.success, name="success"),
]
