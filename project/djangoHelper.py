"""
Python Hilfsmodul für Django
"""

# Modul Informationen
__version__ = "0.1"
__author__ = "Nico Giessmann"

from datetime import datetime
from urllib.parse import urlencode

import db_module
from django.apps import apps
from django.http import HttpResponse, HttpRequest
from django.urls import reverse
from typing import Union


def set_session_cookie(response: HttpResponse, session_id: str, expiry: datetime) -> None:
    """
    Die Funktion setzt einen Session Cookie. Django escaped alle Cookien in doppelten Anführungszeichen.

    :param response: Django HttpResponse
    :param session_id: Session ID
    :return: None
    """
    response.set_cookie("session", session_id, expires=expiry, httponly=True,
                        samesite='Lax')  # TODO: In production, set secure=True
    return response


def get_session_cookie(request: HttpRequest) -> Union[str, bool]:
    """
    Die Funktion liefert den gesetzen Session Cookie.

    :param request: Django HttpRequest
    :return: session id | None
    """
    session_cookie_or_none = request.COOKIES.get("session", None)
    return session_cookie_or_none


def is_active_session(request: HttpRequest) -> Union[int, bool, None]:
    """
    Die Funktion überprüft, ob eine aktive Sessione existiert.

    :param request: Django Http request
    :return: None (wenn keine Session vorliegt), User ID (wenn eine Session vorliegt), False (bei Internal Server Error)
    """
    check_or_session_id = get_session_cookie(request)
    if check_or_session_id != None:
        # Check if cookie is valid
        check_or_user_id = db_module.get_user_id_from_session(check_or_session_id)
        if check_or_user_id == None:
            return None
        elif check_or_user_id == False:
            return False
        else:
            return check_or_user_id


def delete_session(request: HttpRequest, response: HttpResponse) -> None:
    """
    Diese Funktion beendet eine aktive Session und löscht server- und clientseitig Sessiondaten.

    :param request:  Django Http request
    :return: None
    """
    check_or_session_id = get_session_cookie(request)
    if check_or_session_id != None:
        db_module.delete_session(check_or_session_id)
        response.delete_cookie("session")
    # do nothing if there is no session id on client


def access_permitted(request: HttpRequest, user_id: int, allowed_roles: tuple) -> bool:
    """
    Die Funktion überprüft, ob ein Nutzer die Erlaubnis hat eine App zu nutzen. Vorher muss geprüft werden, ob eine
    Session aktiv ist (is_active_session).

    :param user_id: ID des Nutzers
    :param allowed_roles: Tuple der erlaubten Rollen für diese App (siehe AppConfig)
    :return: True | False
    """
    check_or_user_roles = db_module.get_user_roles(user_id)
    if check_or_user_roles == None or check_or_user_roles == False:
        return False
    # Return true if one match found
    for allowed_role in allowed_roles:
        for user_role in check_or_user_roles:
            if allowed_role == user_role: return True
    return False


def get_permitted_apps(request: HttpRequest) -> Union[list, bool]:
    """
    Die Funktion liefert alle zugriffsberechtigten Apps für den Nutzer.

    :param request: Django http response
    :return: list mit Apps | False
    """
    # Get user_roles
    check_or_user_id = db_module.get_user_id_from_session(get_session_cookie(request))
    if check_or_user_id == None or check_or_user_id == False:
        return list()
    check_or_user_roles = db_module.get_user_roles(check_or_user_id)
    if check_or_user_roles == None or check_or_user_roles == False:
        return list()

    permitted_apps = list()

    # List all permitted apps
    for role_id in check_or_user_roles:
        apps_for_role = list()
        for app in apps.get_app_configs():
            if hasattr(app, "roles"):
                if role_id in app.roles:
                    apps_for_role.append((app.verbose_name, reverse(app.name + ":index"), app.icon, app.name))
        permitted_apps.append((get_role_name(role_id), apps_for_role))
    return permitted_apps


def check_query(return_from_server) -> bool:
    """
    Die Funktion prüft, ob eine Datenbankabfrage fehlerhaft war.

    :param return_from_server: Rückgabe aus dem db_module
    :return: True | False
    """
    if type(return_from_server) is int:
        if return_from_server == 0: return True
    if return_from_server == None or return_from_server == False:
        return False
    return True


def get_role_name(role_id: int) -> str:
    """
    Die Funktion gibt den Namen einer Rolle aus einer ID zurück.

    :param role_id: ID der Rolle
    :return: Rollenname
    """
    switcher = {
        db_module.ROLE_FAHRER: "Fahrer",
        db_module.ROLE_CONTROLLER: "Controller",
        db_module.ROLE_DRITTER: "Dritter",
        db_module.ROLE_BUCHHALTER: "Buchhalter"
    }
    return switcher.get(role_id, None)


def get_date(data: str):
    if data == None: return None
    return datetime.date(datetime.strptime(data, "%d.%m.%Y"))


def get_time(data: str):
    if data == None: return None
    return datetime.time(datetime.strptime(data, "%H:%M"))


def get_datetime(date: datetime, time: datetime):
    if date == None or time == None: return None
    return datetime.combine(date, time)


def url_with_parameters(url_name: str, params: dict) -> str:
    base_url = reverse(url_name)  # 1 /products/
    query_string = urlencode(params)  # 2 category=42
    url = '{}?{}'.format(base_url, query_string)  # 3 /products/?category=42
    return url


def get_error_dict(error_code: str, error_title: str, error_msg: str, error_link: str) -> dict():
    error_data = {
        "error_code": error_code,
        "error_title": error_title,
        "error_msg": error_msg,
        "error_link": error_link,
    }
    return error_data
