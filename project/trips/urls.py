from django.urls import path

from . import views
from .apps import TripsConfig

app_name = TripsConfig.name

urlpatterns = [
    path("", views.index, name="index"),
    path("general", views.general, name="general"),
    path("insert_empty_trip", views.insert_empty_trip, name="insert_empty_trip"),
    path("trip_part", views.trip_part, name="trip_part"),
    path("insert_trip_part", views.insert_trip_part, name="insert_trip_part"),
    path("insert_trip_back", views.insert_trip_back, name="insert_trip_back"),
    path("success", views.success, name="success"),
]
