import db_module
from django.apps import AppConfig


class TripsConfig(AppConfig):
    name = 'trips'
    verbose_name = "Fahrt hinzufügen"
    roles = (db_module.ROLE_FAHRER,)
    icon = "fas fa-road"
