import db_module
import djangoHelper
from db_module import better_get, Location, TripPart
from django.http import HttpRequest, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from djangoHelper import check_query, get_date, get_time, get_datetime, url_with_parameters, get_error_dict
from typing import Union

from .apps import TripsConfig


def index(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View für die Startseite von Fahrzeuge hinzufügen.
    """
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, TripsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrten hinzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))
    # Check if user has entered an empty trip
    check_or_trip_parts = db_module.get_saved_trip_parts(session_check)
    if check_query(check_or_trip_parts):
        if check_or_trip_parts >= 0:  # Trip exists
            return HttpResponseRedirect(reverse("trips:trip_part"))

    return render(request, "trips/index.html", {"trips_active": "active"})


def general(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion stellt das Formular für das Hinzufügen einer leeren Fahrt bereit.
    """
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, TripsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrten hinzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    # Check if user has a car
    check_or_car_id = db_module.user_owns_car(session_check)
    if not check_query(check_or_car_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error", get_error_dict("403", "Forbidden",
                                                                                          "Sie besitzen kein Fahrzeug, buchen sie zunächst eines.",
                                                                                          reverse("booking:index"))))

    # Check if user has entered an empty trip
    check_or_trip_parts = db_module.get_saved_trip_parts(session_check)
    if not check_query(check_or_trip_parts):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Fahrtabschnitte.",
                                                                       reverse("dashboard:index"))))
    if check_or_trip_parts >= 0:  # Trip exists
        return HttpResponseRedirect(reverse("trips:trip_part"))

    check_or_car_infos = db_module.get_car_infos(check_or_car_id)
    if not check_query(check_or_car_infos):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfrage von Fahrzeugdaten",
                                                                       reverse("trips:index"))))

    context = {"car": check_or_car_infos, "trips_active": "active"}

    return render(request, "trips/general.html", context)


def insert_empty_trip(request: HttpRequest) -> HttpResponseRedirect:
    """
    Die Funktion fügt eine leere Fahrt hinzu.
    """
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, TripsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrten hinzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    # Check if user has a car
    check_or_car_id = db_module.user_owns_car(session_check)
    if not check_query(check_or_car_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error", get_error_dict("403", "Forbidden",
                                                                                          "Sie besitzen kein Fahrzeug, buchen sie zunächst eines.",
                                                                                          reverse("booking:index"))))

    # Check if user has entered an empty trip
    check_or_trip_parts = db_module.get_saved_trip_parts(session_check)
    if not check_query(check_or_trip_parts):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Fahrtabschnitte.",
                                                                       reverse("dashboard:index"))))
    if check_or_trip_parts >= 0:  # Trip exists
        return HttpResponseRedirect(reverse("trips:trip_part"))

    name = better_get(request, "name")

    # Insert empty trip
    check_or_true = db_module.insert_empty_trip(name, session_check)
    if not check_query(check_or_true):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Hinzufügen einer leeren Fahrt.",
                                                                       reverse("trips: general"))))

    return HttpResponseRedirect(reverse("trips:trip_part"))


def trip_part(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert einen View mit dem Formular zu Erstellung eines Fahrtabschnittes.
    """
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, TripsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrten hinzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    # Check if user has a car
    check_or_car_id = db_module.user_owns_car(session_check)
    if not check_query(check_or_car_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error", get_error_dict("403", "Forbidden",
                                                                                          "Sie besitzen kein Fahrzeug, buchen sie zunächst eines.",
                                                                                          reverse("booking:index"))))

    # Check if user has entered an empty trip
    check_or_trip_parts = db_module.get_saved_trip_parts(session_check)
    if not check_query(check_or_trip_parts):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Fahrtabschnitte.",
                                                                       reverse("dashboard:index"))))
    # Check is empty trip is already saved
    if check_or_trip_parts == -1:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Fahrtabschnitte.",
                                                                       reverse("trips:general"))))

    disabled = check_or_trip_parts < 1

    context = {"form_count": check_or_trip_parts+1, "disabled": disabled, "trips_active": "active"}

    return render(request, "trips/trip_part.html", context)


def insert_trip_part(request: HttpRequest) -> HttpResponseRedirect:
    """
    Die Funktion fügt einen Trip away oder einen Trip between ein.
    """
    # Redirect to insert_back if button is clicked
    button_insert_back = better_get(request, "insert_back")
    if button_insert_back != None:
        return insert_trip_back(request)

    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, TripsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrten hinzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    # Check if user has a car
    check_or_car_id = db_module.user_owns_car(session_check)
    if not check_query(check_or_car_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error", get_error_dict("403", "Forbidden",
                                                                                          "Sie besitzen kein Fahrzeug, buchen sie zunächst eines.",
                                                                                          reverse("booking:index"))))

    # Check if user has entered an empty trip
    check_or_trip_parts = db_module.get_saved_trip_parts(session_check)
    if not check_query(check_or_trip_parts):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Fahrtabschnitte.",
                                                                       reverse("dashboard:index"))))
    # Check is empty trip is already saved
    if check_or_trip_parts == -1:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Fahrtabschnitte.",
                                                                       reverse("trips:general"))))

    startdate = get_date(better_get(request, "startdate"))
    starttime = get_time(better_get(request, "starttime"))
    start_datetime = get_datetime(startdate, starttime)
    enddate = get_date(better_get(request, "enddate"))
    endtime = get_time(better_get(request, "endtime"))
    end_datetime = get_datetime(enddate, endtime)

    start_street = better_get(request, "start_street")
    start_house_number = better_get(request, "start_house_number")
    start_postalcode = better_get(request, "start_postalcode")
    start_town = better_get(request, "start_town")
    start_location = Location(start_street, start_house_number, start_postalcode, start_town)

    end_street = better_get(request, "end_street")
    end_house_number = better_get(request, "end_house_number")
    end_postalcode = better_get(request, "end_postalcode")
    end_town = better_get(request, "end_town")
    end_location = Location(end_street, end_house_number, end_postalcode, end_town)

    distance = better_get(request, "distance")

    trip_part = TripPart(start_datetime, end_datetime, distance, start_location, end_location)

    check_or_trip_id = db_module.get_trip_id(session_check)
    if not check_query(check_or_trip_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen von Fahrtdaten.",
                                                                       reverse("trips:trip_part"))))
    # Check if trip_away
    if check_or_trip_parts == 0:
        part_id = db_module.insert_trip_away(check_or_trip_id, trip_part)
        # TODO: React on errors
    else:
        # else we have a trip between
        db_module.insert_trip_between(check_or_trip_id, trip_part)
        # TODO: React on errors

    return HttpResponseRedirect(reverse("trips:trip_part"))


def insert_trip_back(request: HttpRequest) -> HttpResponseRedirect:
    """
    Die Funktion fügt eine Rückfahrt hinzu und beendet die aktuelle Trip Session.
    """
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, TripsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrten hinzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    # Check if user has a car
    check_or_car_id = db_module.user_owns_car(session_check)
    if not check_query(check_or_car_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error", get_error_dict("403", "Forbidden",
                                                                                          "Sie besitzen kein Fahrzeug, buchen sie zunächst eines.",
                                                                                          reverse("booking:index"))))

    # Check if user has entered an empty trip
    check_or_trip_parts = db_module.get_saved_trip_parts(session_check)
    if not check_query(check_or_trip_parts):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Fahrtabschnitte.",
                                                                       reverse("dashboard:index"))))
    # Check is empty trip is already saved
    if check_or_trip_parts == -1:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen der Fahrtabschnitte.",
                                                                       reverse("trips:general"))))

    # Check if enough trip_parts are saved
    if check_or_trip_parts == 0:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Nicht ausreichend viele Fahrtabschnitte gespeichert.",
                                                                       reverse("trips:trip_part"))))

    startdate = get_date(better_get(request, "startdate"))
    starttime = get_time(better_get(request, "starttime"))
    start_datetime = get_datetime(startdate, starttime)
    enddate = get_date(better_get(request, "enddate"))
    endtime = get_time(better_get(request, "endtime"))
    end_datetime = get_datetime(enddate, endtime)

    start_street = better_get(request, "start_street")
    start_house_number = better_get(request, "start_house_number")
    start_postalcode = better_get(request, "start_postalcode")
    start_town = better_get(request, "start_town")
    start_location = Location(start_street, start_house_number, start_postalcode, start_town)

    end_street = better_get(request, "end_street")
    end_house_number = better_get(request, "end_house_number")
    end_postalcode = better_get(request, "end_postalcode")
    end_town = better_get(request, "end_town")
    end_location = Location(end_street, end_house_number, end_postalcode, end_town)

    distance = better_get(request, "distance")

    trip_part = TripPart(start_datetime, end_datetime, distance, start_location, end_location)
    check_or_trip_id = db_module.get_trip_id(session_check)

    if not check_query(check_or_trip_id):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Abfragen von Fahrtdaten.",
                                                                       reverse("trips:trip_part"))))

    # Insert trip_back and finishing routine
    check = db_module.insert_trip_back(check_or_trip_id, trip_part)
    if not check_query(check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Hinzufügen der Rückfahrt.",
                                                                       reverse("trips:trip_part"))))
    # Got back to success page
    return HttpResponseRedirect(reverse("trips:success"))


def success(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Die Funktion liefert den View zur Darstellung der Success Seite
    """
    session_check = djangoHelper.is_active_session(request)  # Just in case
    if session_check == None or session_check == False:
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("500", "Internal Server Error",
                                                                       "Fehler beim Überprüfen der Session.",
                                                                       reverse("frontpage:login"))))

    # Access check
    access_check = djangoHelper.access_permitted(request, session_check, TripsConfig.roles)
    if not check_query(access_check):
        return HttpResponseRedirect(url_with_parameters("dashboard:error",
                                                        get_error_dict("403", "Forbidden",
                                                                       "Der Nutzer hat nicht die Berechtigungen um Fahrten hinzufügen.",
                                                                       reverse(
                                                                           "dashboard:index"))))

    return render(request, "trips/success.html")
