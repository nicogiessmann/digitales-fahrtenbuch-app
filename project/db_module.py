"""
Python Schnittstellen Modul für die Datenbanken des Digitalen Fahrtenbuches und Fuhrparkmanagements

Mithilfe dieses Moduls lässt sich auf die Datenbank für das
Digitales Fuhrparkmanagement und Fahrtenbuch zugreifen (digitales_fahrtenbuch_db).
Automatisch eingebunden ist auch die Datenbank zur Archivierung (archive_db).
"""

import binascii
import datetime
import hashlib
import os
import re
from datetime import timedelta

import mysql.connector
import random
from django.http import HttpRequest
from mysql.connector import MySQLConnection
from mysql.connector.cursor import MySQLCursor
from typing import Union, Final

# Modul Informationen
__version__ = "0.1"
__author__ = "Nico Giessmann"

# Konfigurationsvariablen
_db_config = {"host": "raspberrypi-server.8s2lmvwnol83snfm.myfritz.net", "port": "80", "raise_on_warnings": True}
_db_config_editor = {"user": "editor", "password": "&f*2gb6*Ay?xD23M"}
_db_config_viewer = {"user": "viewer", "password": "7;7N8*GF/Gmc6gY$"}

# Konstanten
TYPE_KLEINWAGEN: Final = 1
TYPE_CABRIO: Final = 2
TYPE_COUPE: Final = 3
TYPE_GELAENDEWAGEN_SUV: Final = 4
TYPE_LIMOUSINE: Final = 5
TYPE_KOMBI: Final = 6
TYPE_TRANSPORTER: Final = 7
TYPE_KLEINBUS_VAN: Final = 8
TYPE_LASTKRAFTWAGEN: Final = 9
TYPE_MOFA: Final = 10
TYPE_LEICHTKRAFTRAD: Final = 11
TYPE_MOTORRAD: Final = 12

ROLE_FAHRER: Final = 1
ROLE_CONTROLLER: Final = 2
ROLE_BUCHHALTER: Final = 3
ROLE_DRITTER: Final = 4

ATTR_NOT_BLOCKED: Final = 0
ATTR_BLOCKED: Final = 1

BOOKING_GEBUCHT: Final = 1
BOOKING_ABGEHOLT: Final = 2
BOOKING_ZURUECK: Final = 3

SESSION_EXPIRY_DAYS = 1
_SESSION_ID_LEN = 40


# Helper functions
def _connect(user_config: dict) -> MySQLConnection:
    """
    Die Funktion stellt eine Verbindung zur Datenbank als Editor oder Viewer her.
    Als Editor können delete, insert, select und update Anweisungen ausgeführt werden. Als Viewer sind nur select
    Anweisungen möglich.

    :param user_config: Dictionary mit den Indizes users und password, siehe Konfigurationsvariablen
    :return: Aktuelle Datanbankverbindung
    """
    db = mysql.connector.connect(**_db_config, **user_config)
    return db


def _close(db: MySQLConnection) -> None:
    """
    Die Funktion schließt eine aktive MySQL-Verbindung.

    :param db: Aktive Datenbankverbindung
    :return: None
    """
    if db is None:
        return
    db.close()


def _use_digitales_fahrtenbuch_db(cursor: MySQLCursor) -> None:
    """
    Die Funktion wechselt auf die Datenbank 'digitales_fahrtenbuch_db'.

    :param cursor: Aktueller Cursor auf aktive Datenbankverbindung
    :return: None
    """
    sql = "USE digitales_fahrtenbuch_db"
    cursor.execute(sql)


def _set_timezone_berlin(cursor: MySQLCursor) -> None:
    """
    Die Funktion setzt die Zeitzone der aktuellen Abfrage auf Europe/Berlin.

    :param cursor: Aktueller Cursur auf aktive Datenbankverbindung
    :return: None
    """

    sql = "SET time_zone = 'Europe/Berlin';"
    cursor.execute(sql)


def _use_archive_db(cursor: MySQLCursor) -> None:
    """
    Die Funktion wechselt auf die Datenbank 'archive_db'.

    :param cursor: Aktueller Cursor auf aktive Datenbankverbindung
    :return: None
    """
    sql = "USE archive_db"
    cursor.execute(sql)


def _is_empty(test_str: str) -> bool:
    """
    Die Funktion prüft, ob ein String leer ist.

    :param test_str: Zu testender String
    :return: True | False
    """
    if test_str is None:
        return True
    elif type(test_str) is str and len(test_str.replace(" ", "")) == 0:
        return True
    return False


def _hash_str(password):
    """Hash a password for storing. (c) Alessandro Molina 2018"""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def _verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by users. (c) Alessandro Molina 2018"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password


def _gen_random_charset(len: int) -> str:
    """
    Generiert eine zufällige ASCII (7-Bit) Zeichenkette.

    :param len: Länge der Zeichenkette
    :return: Zeichenkette
    """

    charset = ""
    i = 0
    while i < len:
        c_dec = random.randint(33, 126)  # inclusive
        c = chr(c_dec)
        charset += c
        i += 1
    return charset


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# Select functions
def get_car_types() -> list:
    car_list = [
        (TYPE_KLEINWAGEN, "Kleinwagen"),
        (TYPE_CABRIO, "Cabrio"),
        (TYPE_COUPE, "Coupé"),
        (TYPE_GELAENDEWAGEN_SUV, "Geländewagen/SUV"),
        (TYPE_LIMOUSINE, "Limousine"),
        (TYPE_KOMBI, "Kombi"),
        (TYPE_TRANSPORTER, "Transporter"),
        (TYPE_KLEINBUS_VAN, "Kleinbus/VAN"),
        (TYPE_LASTKRAFTWAGEN, "Lastkraftwagen"),
        (TYPE_MOFA, "Mofa"),
        (TYPE_LEICHTKRAFTRAD, "Leichtkraftrad"),
        (TYPE_MOTORRAD, "Motorad"),
    ]
    return car_list


def verify_user(email: str, password: str) -> Union[int, None, bool]:
    """
    Die Funktion überprüft einen Anmeldeversuch des Nutzers.

    :param email: E-Mail-Adresse des Nutzers (verpflichtend)
    :param password: Passwort des Nutzers im Klartext (verpflichtend)
    :return: user_id | None | False
    """

    db = None
    try:
        db = _connect(_db_config_viewer)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        sql = "SELECT `user_id`, `password` FROM `user` WHERE `email`=%s;"
        cursor.execute(sql, (email,))
        result = cursor.fetchall()

        if cursor.rowcount == 1:
            # user exists, compare passwords
            if _verify_password(result[0][1], password):
                # verification success, return user_id
                return result[0][0]
            else:
                # verification failed
                return None
        else:
            # verification failed
            return None

        _close(db)
    except mysql.connector.Error as err:
        print("Fehler beim Überprüfen des Nutzers: " + err.msg)
        _close(db)
        return False


def get_user_id_from_session(session_id: str) -> Union[int, None, bool]:
    """
    Die Funktion überprüft, ob eine valide Session ID existiert und gibt den Nutzer zurück.

    :param session_id: Zu prüfende Session ID
    :return: user_id | None | False
    """

    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)
        _set_timezone_berlin(cursor)

        # Delete session id if expired
        sql = "DELETE FROM `sessions` WHERE `session_id`=%s AND `expiry` < NOW();"
        cursor.execute(sql, (session_id,))

        # Check if user has a valid session id
        sql = "SELECT `user_id` FROM `sessions`WHERE `session_id`=%s;"
        cursor.execute(sql, (session_id,))
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            # Valid session id exists, return user_id
            _close(db)
            return result[0][0]
        # No session id exists, return None
        _close(db)
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Überprüfen der Session ID: " + err.msg)
        _close(db)
        return False


def get_user_infos(user_id: int) -> Union[tuple, None, bool]:
    """
    Die Funktion liefert Infos zum Nutzer. Dazu gehören Vorname, Nachname, E-Mail und Institution.

    :param user_id: ID des Nutzers.
    :return: Tuple(Vorname, Nachname, E-Mail, Institution, Institution_ID), None, False
    """
    db = None
    try:
        db = _connect(_db_config_viewer)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        sql = "SELECT `first_name`, `last_name`, `email`, `title`, `institution_id` FROM `user` INNER JOIN `institution` ON `user`.`institution` = `institution`.`institution_id` WHERE `user_id`=%s;"
        cursor.execute(sql, (user_id,))
        result = cursor.fetchall()

        if cursor.rowcount == 1:
            _close(db)
            return result[0]

        _close(db)
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Abfragen von Nutzerdaten: " + err.msg)
        _close(db)
        return False


def get_cars_of_institution(institution_id: int) -> Union[list, None, bool]:
    """
    Die Funktion gibt die Autos einer Institution zurück.

    :param institution_id: ID der Institution
    :return: List(Tuple(model, manufacturer, vin, seats, milage, first_registration, type, type_id)), None, False (bei Fehlern)
    """
    db = None
    try:
        db = _connect(_db_config_viewer)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Check which cars are available
        sql = "SELECT `car_id`, `model`, `manufacturer`, `vin`, `seats`, `milage`, `first_registration`, `name`, `car`.`type_id` FROM `car` INNER JOIN `car_type` ON `car`.`type_id` = `car_type`.`type_id` WHERE `institution_id` = %s AND `car_id` NOT IN (SELECT `car_id` FROM `booking` WHERE `state_id` != %s);"

        val = (institution_id, BOOKING_ZURUECK,)
        cursor.execute(sql, val)
        result = cursor.fetchall()
        if cursor.rowcount > 0:
            _close(db)
            return result;
        raise Exception("Es konnten keine Fahrzeuge für die Institution gefunden werden.")
    except Exception as err:
        print(bcolors.WARNING + "Fehler beim Abfragen von Fahrzeugdaten: " + str(err) + bcolors.ENDC)
        _close(db)
        return False


def get_user_roles(user_id: int) -> Union[list, None, bool]:
    """
    Diese Funktion liefert alle Rolle des Nutzers.

    :param user_id: ID des Nutzers.
    :return: Liste (Rollen), None, False (bei Fehlern)
    """
    db = None
    try:
        db = _connect(_db_config_viewer)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        sql = "SELECT `role_id` FROM `has_role` WHERE `user_id` = %s;"
        cursor.execute(sql, (user_id,))
        result = cursor.fetchall()
        if cursor.rowcount > 0:
            # User is assigned to a role
            role_ids = list()
            for entry in result:
                role_ids.append(entry[0])
            _close(db)
            return role_ids
        _close(db)
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Abfragen der Rollen: " + err.msg)
        _close(db)
        return False


def user_owns_car(user_id: int) -> Union[int, None, bool]:
    """
    Die Funktion prüft, ob ein Nutzer sein Fahrzeug abgeholt hat und gibt wenn zutreffend die ID des Fahrzeuges zurück.

    :param user_id: ID des Nutzers
    :return: car_id | None | False (bei Fehlern)
    """
    db = None
    try:
        db = _connect(_db_config_viewer)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        sql = "SELECT `car_id` FROM `booking` WHERE `user_id`=%s AND `state_id`=%s;"
        val = (user_id, BOOKING_ABGEHOLT,)
        cursor.execute(sql, val)
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            return result[0][0]
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Abfragen der Fahrzeug ID aus Booking: " + err.msg)
        _close(db)
        return False


def get_car_infos(car_id: int) -> Union[tuple, None, bool]:
    """
    Die Funktion liefert alle Informationen zu einem Auto.

    :param car_id: ID des Fahrzeuges
    :return: Tuple(model, manufacturer, vin, seats, milage, first_registration, type, type_id), None, False (bei Fehlern)
    """
    db = None
    try:
        db = _connect(_db_config_viewer)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        sql = "SELECT `model`, `manufacturer`, `vin`, `seats`, `milage`, `first_registration`, `name`, " \
              "`car`.`type_id` FROM `car` INNER JOIN `car_type` ON `car`.`type_id` = `car_type`.`type_id` WHERE " \
              "`car_id`=%s;"
        cursor.execute(sql, (car_id,))
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            return result[0]
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Abfragen der Fahrzeug der Fahrzeugdaten: " + err.msg)
        _close(db)
        return False


def get_saved_trip_parts(user_id: int):
    """
    Die Funktion ermittelt, wieviele Fahrtabschnitte der Nutzer bereits abgespeichert hat.

    :param user_id: ID des Nutzers
    :return: -1 (Es existiert zur Buchung keine leere Fahrt) | n (Es existiert eine leere Fahrt mit n Fahrtabschnitten) | None | False (bei Fehlern)
    """
    db = None
    try:
        db = _connect(_db_config_viewer)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Check if user has an running booking
        sql = "SELECT `booking_id` FROM `booking` WHERE `user_id`=%s AND `state_id`=%s;"
        val = (user_id, BOOKING_ABGEHOLT,)
        cursor.execute(sql, val)
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            booking_id = result[0][0]
            # Check if user has saved an emptry trip, TODO: Can trip_back_id be set at this point -> Zurückgegeben?
            sql = "SELECT `trip_id`, `trip_away_id` FROM `trip` WHERE `booking_id`=%s;"
            cursor.execute(sql, (booking_id,))
            result = cursor.fetchall()
            if cursor.rowcount == 1:
                # Count trip_parts
                trip_parts = 0
                if result[0][1] != None:
                    trip_parts += 1  # Trip_away is saved
                    # Check how many trip_betweens are saved
                    sql = "SELECT * FROM `trip_between` WHERE `trip_id`=%s;"
                    cursor.execute(sql, (result[0][0],))
                    cursor.fetchall()
                    trip_parts += cursor.rowcount
                    return trip_parts
                return trip_parts  # 0
            return -1
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Abfragen der Fahrt und Fahrtabschnitte: " + err.msg)
        _close(db)
        return False


def get_trip_id(user_id: int) -> Union[int, None, bool]:
    """
    Die Funktion ermittelt die aktuelle Trip ID des Nutzers.

    :param user_id: ID des Nutzers
    :return: id | None | False
    """
    db = None
    try:
        db = _connect(_db_config_viewer)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Check if user has an running booking
        sql = "SELECT `booking_id` FROM `booking` WHERE `user_id`=%s AND `state_id`=%s;"
        val = (user_id, BOOKING_ABGEHOLT,)
        cursor.execute(sql, val)
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            booking_id = result[0][0]
            # Check if user has saved an trip, TODO: Can trip_back_id be set at this point -> Zurückgegeben?
            sql = "SELECT `trip_id` FROM `trip` WHERE `booking_id`=%s;"
            cursor.execute(sql, (booking_id,))
            result = cursor.fetchall()
            if cursor.rowcount == 1:
                return result[0][0]
            return None
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Abfragen der Fahrt ID: " + err.msg)
        _close(db)
        return False


# Insert functions
def insert_car(model: str, manufacturer: str, vin: str, seats: int, milage: int, first_registration: datetime,
               type_id: int, institution_id: int) -> Union[int, Exception]:
    """
    Die Funktion fügt ein neues Auto zur Datenbank hinzu.

    :param model: Modellbezeichnung (verpflichtend)
    :param manufacturer: Hersteller (verpflichtend)
    :param vin: Fahrzeug-Identifikationsnummer (verpflichtend)
    :param seats: Anzahl Sitze (verpflichtend)
    :param milage: Kilometerstand (verpflichtend)
    :param first_registration: Datum der Erstanmeldung (verpflichtend)
    :param type_id: ID des Fahrzeugtyps, siehe TYPE_ Konstanten (verpflichtend)
    :param institution_id: ID der Institution
    :return: ID des hinzugefügten Fahrzeuges | False
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Checks
        if _is_empty(model) or not model.replace(" ", "").isalpha() or len(model) > 50:
            raise ValueError("Der Modellname darf nur Buchstaben oder Leerzeichen enthalten und nicht länger als 50 "
                             "Zeichen lang sein. Er ist verpflichtend anzugeben.")
        if _is_empty(manufacturer) or not manufacturer.replace(" ", "").isalpha() or len(manufacturer) > 50:
            raise ValueError("Der Hersteller darf nur Buchstaben enthalten und nicht länger als 50 Zeichen lang sein. "
                             "Er ist verplichtend anzugeben.")
        if _is_empty(vin) or not vin.isalnum() or not len(vin) == 17:
            raise ValueError("Die Fahrzeug-Identifikationsnummer darf nur Buchstaben und Zahlen enthalten und muss "
                             "genau 17 Zeichen lang sein. Sie ist verplichtend anzugeben.")
        if _is_empty(seats):
            raise ValueError("Die Anzahl der Sitze ist verpflichtend anzugeben.")
        if _is_empty(milage):
            raise ValueError("Der Kilometerstand ist verpflichtend anzugeben.")
        if _is_empty(first_registration) and type(first_registration) == datetime.date:
            raise ValueError(
                "Das Datum der Erstanmeldung ist verplfichtend anzugeben und muss vom Type datetime.date sein.")
        if _is_empty(type_id):
            raise ValueError("Die ID des Fahrzeugtyps ist verpflichtend anzugeben.")
        if _is_empty(institution_id):
            raise ValueError("Die ID der Institution ist verpflichtend anzugeben.")

        sql = "INSERT INTO `car` (`model`, `manufacturer`, `vin`, `seats`, `milage`, `first_registration`, \
                `type_id`, `institution_id`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"
        val = (model, manufacturer, vin, seats, milage, first_registration, type_id, institution_id,)
        cursor.execute(sql, val)
        db.commit()
        car_id = cursor.lastrowid
        _close(db)
        return car_id
    except Exception as err:
        print(bcolors.WARNING + "Fehler beim Hinzufügen eines Fahrzeuges: " + str(err) + bcolors.ENDC)
        _close(db)
        return False


def insert_location(street: str, house_number: str, town: str, postalcode: str) -> Union[int, bool]:
    """
    Die Funktion fügt einen neunen Ort hinzu.

    :param street: Strasse (verpflichtend)
    :param house_number: Hausnummer (optional)
    :param town: Stadt (verpflichtend)
    :param postalcode: Postleitzahl (verpflichtend)
    :return: ID des hinzugefügten Ortes | False
    """

    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)
        # Checks
        if _is_empty(street) or len(street) > 100:
            print("Der Straßenname darf nicht länger als 100 Zeichen sein. Er ist verpflichtend anzugeben.")
            _close(db)
            return False
        if house_number is not None:
            if not house_number.isalnum() or len(house_number) > 5:
                print("Die Hausnummer darf nicht länger als 5 Zeichen sein.")
                _close(db)
                return False
        if _is_empty(town) or len(town) > 60:
            print("Der Stadtname darf nicht länger als 60 Zeichen sein. Er ist verpflichtend anzugeben.")
            _close(db)
            return False
        if _is_empty(postalcode) or not postalcode.isnumeric() or not len(postalcode) == 5:
            print("Die Postleitzahl muss genau 5 Zahlen lang sein. Er ist verpflichtend anzugeben.")
            _close(db)
            return False

        sql = "INSERT INTO `location` (`street`, `house_number`, `town`, `postalcode`) VALUES (%s, %s, %s, %s);"
        val = (street, house_number, town, postalcode,)
        # Auto encodes unicode ;)
        cursor.execute(sql, val)
        location_id = cursor.lastrowid
        db.commit()
        _close(db)
        print("Neuen Ort erfolgreich hinzugefügt.")
        return location_id
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen eines Ortes: " + err.msg)
        _close(db)
        return False


def insert_institution(title: str, location_id: int) -> Union[int, bool]:
    """
    Die Funktion fügt eine Institution hinzu.

    :param title: Titel/Name der Institution (verpflichtend)
    :param location_id: ID eines Ortes (verpflichtend)
    :return: ID der hinzugefügten Institution | False
    """

    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Checks
        if len(title) > 100 or _is_empty(title):
            print("Der Titel einer Institution darf höchstens 100 Zeichen lang sein. Er ist verpflichtend anzugeben.")
            _close(db)
            return False
        if _is_empty(location_id):
            print("Die ID eines Ortes ist verpflichtend anzugeben.")
            _close(db)
            return False

        sql = "INSERT INTO `institution` (`title`, `location`) VALUES (%s, %s);"
        val = (title, location_id,)
        cursor.execute(sql, val)
        institution_id = cursor.lastrowid
        db.commit()
        _close(db)
        print("Neue Institution erfolgreich hinzugefügt.")
        return institution_id
    except mysql.connector.Error as err:
        print(err)
        _close(db)
        return False


def insert_user(first_name: str, last_name: str, email: str, password: str, is_blocked: bool, institution_id: int) -> \
        Union[int, None, bool]:
    """
    Die Funktion fügt einen neuen Nutzer hinzu.

    :param first_name: Vorname (verpflichtend)
    :param last_name: Lastname (verpflichtend)
    :param email: E-Mail-Adresse (verpflichtend)
    :param password: Passswort im Freitext (verpflichtend)
    :param is_blocked: Nutzerblockierung, siehe ATTR_ (optional, standardmäßig nicht blockiert)
    :param institution_id: ID einer Institution (verplichtend)
    :return: ID des hinzugefügten Nutzers | None | False
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Check if user already exists
        sql = "SELECT * FROM `user` WHERE `email`=%s;"
        cursor.execute(sql, (email,))
        cursor.fetchall()
        if cursor.rowcount != 0:
            # User already exists
            return None

        # Checks
        if _is_empty(first_name) or not first_name.replace(" ", "").isalpha() or len(first_name) > 50:
            raise ValueError("Der Vorname darf nur aus Buchstaben oder Leerzeichen bestehen und maximal 50 "
                             "Buchstaben lang sein. Er ist verpflichtend anzugeben.")
        if _is_empty(last_name) or not last_name.replace(" ", "").isalpha() or len(last_name) > 50:
            raise ValueError(
                "Der Nachname darf nur aus Buchstaben oder Leerzeichen bestehen und maximal 50 Buchstaben lang "
                "sein. Er ist verpflichtend anzugeben.")
        if _is_empty(email) or len(email) > 254 or not re.search("^.+@.+\..+$", email) or " " in email:
            raise ValueError(
                "Die E-Mail muss einem gültigen Format entsprechen und darf maximal 254 Zeichen lang sein. Sie ist "
                "verpflichtend anzugeben.")
        if _is_empty(password) or " " in password:
            raise ValueError("Das Passwort darf keine Leerzeichen enthalten. Es ist verplichtend anzugeben.")
        if _is_empty(institution_id):
            raise ValueError("Eine Institution ist verpflichtend anzugeben.")

        # Password hashing
        hash_pw = _hash_str(password)

        sql = "INSERT INTO `user` (`first_name`, `last_name`, `email`, `password`, `is_blocked`, `institution`) " \
              "VALUES (%s, %s, %s, %s, %s, %s);"
        val = (first_name, last_name, email, hash_pw, is_blocked, institution_id,)
        cursor.execute(sql, val)
        db.commit()
        user_id = cursor.lastrowid
        _close(db)
        return user_id
    except Exception as err:
        print(bcolors.WARNING + "Fehler beim Hinzufügen eines Nutzers: " + str(err) + bcolors.ENDC)
        _close(db)
        return False


def insert_user_role(user_id: int, role_id) -> bool:
    """
    Die Funktion ordnet eines Nutzer eine Rolle zu.

    :param user_id: ID des Nutzers
    :param role_id: ID der Rolle
    :return: True | False (bei Fehlern)
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        sql = "INSERT INTO `has_role` (`user_id`, `role_id`) VALUES (%s, %s);"
        val = (user_id, role_id,)
        cursor.execute(sql, val)
        db.commit()
        _close(db)
        return True
    except Exception as err:
        print(bcolors.WARNING + "Fehler beim Hinzufügen einer Rolle: " + str(err) + bcolors.ENDC)
        _close(db)
        return False


def insert_booking(start_datetime: datetime, end_datetime: datetime, car_id: int, user_id: int) -> Union[int, bool]:
    """
    Die Funktion fügt eine neue Buchung hinzu.

    :param start_datetime: Startzeit der Buchung (verpflichtend)
    :param end_datetime: Endzeit der Buchung (verpflichtend)
    :param car_id: ID eines Fahrzeuges (verpflichtend)
    :param user_id: ID eines Nutzers (verpflichtend)
    :return: ID der erstellten Buchung | False
    """

    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Checks
        if _is_empty(start_datetime):
            raise ValueError("Es muss eine Startzeit der Buchung angegeben werden.")
        if _is_empty(end_datetime) and start_datetime < end_datetime:
            raise ValueError("Die Endzeit muss später als die Startzeit liegen und ist verpflichtend anzugeben.")
        if _is_empty(car_id):
            raise ValueError("Die Angabe eines Fahrzeuges ist verpflichtend.")
        if _is_empty(user_id):
            raise ValueError("Die Angabe eines Nutzers ist verpflichtend.")

        sql = "INSERT INTO `booking` (`start_datetime`, `end_datetime`, `car_id`, `user_id`) VALUES (%s, " \
              "%s, %s, %s);"
        val = (start_datetime, end_datetime, car_id, user_id,)
        cursor.execute(sql, val)
        db.commit()
        booking_id = cursor.lastrowid
        _close(db)
        return booking_id
    except mysql.connector.Error as err:
        print(bcolors.WARNING + "Fehler beim Hinzufügen einer neuen Buchung: " + err.msg + bcolors.ENDC)
        _close(db)
        return False


def set_booking_state(booking_id: int, booking_state: int) -> bool:
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        sql = "UPDATE `booking` SET `state_id`=%s WHERE `booking_id`=%s"
        cursor.execute(sql, (booking_state, booking_id,))
        db.commit()
        return True
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen einer neuen Buchung: " + err.msg)
        _close(db)
        return False


class Location:
    def __init__(self, street: str, house_number: str, postalcode: str, town: str):
        # TODO: Checks and make funciton insert_location have attribute Location
        # Init
        self.street = street
        self.house_number = house_number
        self.postalcode = postalcode
        self.town = town


class TripPart:
    """
    Die Klasse TripPart speichert einen Fahrtabschnitt
    """

    def __init__(self, start_datetime: datetime, end_datetime: datetime, distance: float, start_location: Location,
                 end_location: Location):
        """
        Konstruktor

        :param start_datetime: Startzeit des Fahrtabschnittes
        :param end_datetime: Endzeit des Fahrtabschnittes
        :param distance: Zurückgelegte Entfernung
        :param start_location_id: ID des Startortes
        :param end_location_id: ID des Endortes
        """
        # Checks
        """
        if _is_empty(start_datetime) or start_datetime is not datetime:
            raise TypeError("Die Startzeit muss einem validen Format entsprechen und ist verpflichtend anzugeben.")
        if _is_empty(end_datetime) or end_datetime is not datetime:
            raise TypeError("Die Endzeit muss einem validen Format entsprechen und ist verpflichtend anzugeben.")
        if start_datetime > end_datetime:
            raise ValueError("Die Startzeit muss vor der Endzeit liegen.")
        if _is_empty(distance):
            raise TypeError("Die Distanz ist verpflichtend anzugeben.")
        if distance <= 0.0:
            raise ValueError("Die Distanz muss größer als 0.0 sein.")
        if _is_empty(start_location_id):
            raise TypeError("Der Startort muss verpflichtend angegeben werden.")
        if start_location_id <= 0:
            raise ValueError("Der Startort muss eine valide ID besitzen.")
        if _is_empty(end_location_id):
            raise TypeError("Der Endort muss verpflichtend angegeben werden")
        if end_location_id <= 0:
            raise ValueError("Der Endort muss eine valide ID besitzen.")
        if start_location_id == end_location_id:
            raise ValueError("Der Startort und der Endort dürfen nicht identisch sein.")
        """

        # Init
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime
        self.distance = distance
        self.start_location = start_location
        self.end_location = end_location


def insert_trip(name: str, trip_away: TripPart, trip_back: TripPart, *trips_between: TripPart, user_id: int,
                car_id: int, booking_id: int) -> Union[int, bool]:
    """
    Die Funktion fügt eine neue Fahrt hinzu.

    :param name: Bezeichnung der Fahrt (optional)
    :param trip_away: Hinfahrt als Fahrtabschnitt
    :param trip_back: Rückfahrt als Fahrtabschnitt
    :param trips_between: Beliebig viele Zwischenstopps als Tupel von Fahrtabschnitten
    :param user_id: ID des Fahrers
    :param car_id: ID des Fahrzeuges
    :param booking_id: ID der Buchung
    :return: ID der hinzugefügten Fahrt | False
    """

    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Checks
        if _is_empty(trip_away):
            print("Es muss eine Hinfahrt angegeben werden.")
            _close(db)
            return False
        if _is_empty(trip_back):
            print("Es muss eine Rückfahrt angegeben werden.")
            _close(db)
            return False

        # Insert Trip_Parts
        sql = "INSERT INTO `trip_part` (`start_datetime`, `end_datetime`, `distance`, `start_location_id`, " \
              "`end_location_id`) VALUES (%s, %s, %s, %s, %s);"

        # For Trip_Away
        val = (trip_away.start_datetime, trip_away.end_datetime, trip_away.distance, trip_away.start_location_id,
               trip_away.end_location_id,)
        cursor.execute(sql, val)
        trip_away_id = cursor.lastrowid

        # For Trip_Back
        val = (trip_back.start_datetime, trip_back.end_datetime, trip_back.distance, trip_back.start_location_id,
               trip_back.end_location_id,)
        cursor.execute(sql, val)
        trip_back_id = cursor.lastrowid

        # For Trips_Between
        trips_between_ids = []
        for trip_between in trips_between:
            val = (trip_between.start_datetime, trip_between.end_datetime, trip_between.distance,
                   trip_between.start_location_id, trip_between.end_location_id,)
            cursor.execute(sql, val)
            trips_between_ids.append(cursor.lastrowid)

        # Insert Trip_Away
        sql = "INSERT INTO `trip_away` (`part_id`) VALUES (%s);"
        cursor.execute(sql, trip_away_id)

        # Insert Trip_Back
        sql = "INSERT INTO `trip_back` (`part_id`) VALUES (%s);"
        cursor.execute(sql, trip_back_id)

        # Insert Trip
        sql = "INSERT INTO `trip` (`name`, `trip_away_id`, `trip_back_id`, `user_id`, `car_id`, `booking_id`) VALUES " \
              "(%s, %s, %s, %s, %s, %s);"
        val = (name, trip_away_id, trip_back_id, user_id, car_id, booking_id)
        cursor.execute(sql, val)
        trip_id = cursor.lastrowid

        # Insert Trip_Between
        sql = "INSERT INTO `trip_between` (`part_id`, `trip_id`) VALUES (%s, %s);"
        for trip_between_id in trips_between_ids:
            cursor.execute(sql, (trip_between_id, trip_id,))

        db.commit()
        _close(db)
        print("Neuen Trip erfolgreich hinzugefügt.")
        return trip_id
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen eines Fahrtabschnittes: " + err)
        _close(db)
        return False


def insert_empty_trip(name: str, user_id: int) -> Union[bool, None]:
    """
    Die Funktion fügt eine leere Fahrt hinzu und setzt die entsprechende Session.

    :param name: Name der Fahrt
    :param user_id: ID des Nutzers
    :return: True | None | False (bei Fehlern)
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # get open booking for this user TODO: Trigger that prevents user from having multiple bookings with status 2
        sql = "SELECT `booking_id`, `car_id` FROM `booking` WHERE `user_id`=%s AND `state_id`=%s;"
        cursor.execute(sql, (user_id, BOOKING_ABGEHOLT))
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            # insert empty booking
            sql = "INSERT INTO `trip` (`name`, `car_id`, `booking_id`) VALUES (%s, %s, %s);"
            val = (name, result[0][1], result[0][0],)
            cursor.execute(sql, val)
            trip_id = cursor.lastrowid
            # add trip_id to session
            sql = "UPDATE `sessions` SET `trip_id`=%s WHERE `user_id`=%s;"
            cursor.execute(sql, (trip_id, user_id,))
            db.commit()
            return True
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen einer Fahrt: " + err.msg)
        _close(db)
        return False


def insert_trip_part(trip_part: TripPart) -> Union[bool, int]:
    """
    Die Funktion fügt einen Trip Part ein
    :param trip_part: Trip Part
    :return: Trip Part Id | False
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Insert Locations
        start_location_id = insert_location(trip_part.start_location.street, trip_part.start_location.house_number,
                                            trip_part.start_location.town, trip_part.start_location.postalcode)
        end_location_id = insert_location(trip_part.end_location.street, trip_part.end_location.house_number,
                                          trip_part.end_location.town, trip_part.end_location.postalcode)

        sql = "INSERT INTO `trip_part` (`start_datetime`, `end_datetime`, `distance`, `start_location_id`, " \
              "`end_location_id`) VALUES (%s, %s, %s, %s, %s);"
        val = (
            trip_part.start_datetime, trip_part.end_datetime, trip_part.distance, start_location_id, end_location_id,)
        cursor.execute(sql, val)
        db.commit()
        trip_part_id = cursor.lastrowid
        _close(db)
        return trip_part_id
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen eines Fahrtabschnittes: " + err.msg)
        _close(db)
        return False


def insert_trip_away(trip_id: int, trip_away: TripPart) -> bool:
    """
    Die Funktion fügt einen Trip Away ein.

    :param trip_id: ID des zugehörigen Trips
    :param trip_away: Trip Away
    :return: True | False (bei Fehlern)
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # Insert trip part
        trip_part_id = insert_trip_part(trip_away)

        # insert trip away
        sql = "INSERT INTO `trip_away` (`part_id`) VALUES (%s);"
        cursor.execute(sql, (trip_part_id,))

        # Insert into trip
        sql = "UPDATE `trip` SET `trip_away_id`=%s WHERE `trip_id`=%s;"
        val = (trip_part_id, trip_id,)
        cursor.execute(sql, val)
        db.commit()
        _close(db)
        return True
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen der Hinfahrt: " + err.msg)
        _close(db)
        return False


def insert_trip_between(trip_id: int, trip_between: TripPart) -> bool:
    """
    Die Funktion fügt einen Trip Between ein.

    :param trip_id: ID des Trips
    :param trip_between: Trip Between
    :return: True | False
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        trip_part_id = insert_trip_part(trip_between)
        # Insert into Trip_between
        sql = "INSERT INTO `trip_between` (`part_id`, `trip_id`) VALUES (%s, %s);"
        val = (trip_part_id, trip_id,)
        cursor.execute(sql, val)
        db.commit()
        _close(db)
        return True
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen der Hinfahrt: " + err.msg)
        _close(db)
        return False


def insert_trip_back(trip_id: int, trip_back: TripPart) -> Union[bool, None]:
    """
        Die Funktion fügt einen Trip Back ein, setzt die Buchung auf zurückgegeben und löscht den Trip aus der Session.

        :param trip_id: ID des zugehörigen Trips
        :param trip_away: Trip Back
        :return: True | None | False (bei Fehlern)
        """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        trip_part_id = insert_trip_part(trip_back)

        # insert trip away
        sql = "INSERT INTO `trip_back` (`part_id`) VALUES (%s);"
        cursor.execute(sql, (trip_part_id,))

        # Insert into trip
        sql = "UPDATE `trip` SET `trip_back_id`=%s WHERE `trip_id`=%s;"
        val = (trip_part_id, trip_id,)
        cursor.execute(sql, val)

        # Get booking_id
        sql = "SELECT `booking_id` FROM `trip` WHERE `trip_id`=%s;"
        cursor.execute(sql, (trip_id,))
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            # Update booking state
            booking_id = result[0][0]
            sql = "UPDATE `booking` SET `state_id`=%s WHERE `booking_id`=%s;"
            cursor.execute(sql, (BOOKING_ZURUECK, booking_id,))
            # Get user_id from booking
            sql = "SELECT `user_id` FROM `booking` WHERE `booking_id`=%s;"
            cursor.execute(sql, (booking_id,))
            result = cursor.fetchall()
            if cursor.rowcount == 1:
                user_id = result[0][0]
                # Trip aus Session löschen
                sql = "UPDATE `sessions` SET `trip_id`=NULL WHERE `user_id`=%s;"
                cursor.execute(sql, (user_id,))
                db.commit()
                _close(db)
                return True
            db.commit()
            _close(db)
            return None
        db.commit()
        _close(db)
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen der Rückfahrt: " + err.msg)
        _close(db)
        return False


def better_get(request: HttpRequest, attr: str) -> Union[str, None]:
    """
    Eine optimierte get-Methode für Django request-Objekte, die None anstatt eines leeren Strings zurückgibt

    :param request: Django request Objekt
    :param attr: Name des Input-Elementes
    :return: Input | None
    """

    value = request.POST.get(attr)
    if value == "":
        return None
    else:
        return value


def create_new_session(user_id: int) -> Union[tuple, bool]:
    """
    Diese Funktion erstellt für einen Nutzer eine neue Session ID.
    Wichtig: Vorher überprüfen, ob Nutzer auch wirklich existiert und ob das Passwort korrekt ist.
    Wenn bereits eine Session ID existieren sollte, wird diese gelöscht.

    :param user_id: ID des Nutzers
    :return: tuple(Session ID, expiry_date) | False
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)
        _set_timezone_berlin(cursor)

        # Delete all expired sessionids for this user
        sql = "DELETE FROM `sessions` WHERE `user_id`=%s AND `expiry` < NOW();"
        cursor.execute(sql, (user_id,))

        # TODO: Delete Trips with all Trip_Parts if Trip is not completed

        # Check if user has a valid session_id already
        sql = "SELECT `session_id`, `expiry` FROM `sessions` WHERE `user_id`=%s;"
        cursor.execute(sql, (user_id,))
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            return result[0]

        # Create and insert new sessionid
        session_id = _gen_random_charset(_SESSION_ID_LEN) + str(user_id)
        expiry = datetime.datetime.now() + timedelta(days=SESSION_EXPIRY_DAYS)
        sql = "INSERT INTO `sessions`(`session_id`, `user_id`, `expiry`) VALUES (%s, %s, %s);"
        val = (session_id, user_id, expiry,)
        cursor.execute(sql, val)
        db.commit()
        _close(db)
        return (session_id, expiry)
    except mysql.connector.Error as err:
        print("Fehler beim Hinzufügen einer Session: " + err.msg)
        _close(db)
        return False


def delete_session(session_id: str) -> Union[None, bool]:
    """
    Diese Funktion löscht eine aktive Session serverseitig.

    :param session_id: Session ID
    :return: None | False (bei Fehlern)
    """

    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        sql = "DELETE FROM `sessions` WHERE `session_id`=%s;"
        cursor.execute(sql, (session_id,))
        db.commit()
        _close(db)
    except mysql.connector.Error as err:
        print("Fehler beim Entfernen einer Session: " + err.msg)
        _close(db)
        return False


def change_password(user_id: int, old_pass: str, new_pass: str) -> Union[bool, None]:
    """
    Die Funktion ändert das Passwort des Nutzers.

    :param user_id: ID des Nutzers
    :param old_pass: Altes Passwort
    :param new_pass: Neues Passwort
    :return: True (für erfolgreiche Änderung), False (für Fehler), None (für fehlerhafte Eingaben)
    """
    db = None
    try:
        db = _connect(_db_config_editor)
        cursor = db.cursor()
        _use_digitales_fahrtenbuch_db(cursor)

        # get real password
        sql = "SELECT `password` FROM `user` WHERE `user_id`=%s;"
        cursor.execute(sql, (user_id,))
        result = cursor.fetchall()
        if cursor.rowcount == 1:
            real_pass = result[0][0]
            if _verify_password(real_pass, old_pass) and old_pass != new_pass:
                # Checks completed, update password TODO: Implement criteria for password security
                sql = "UPDATE `user` SET `password`=%s WHERE `user_id`=%s"
                val = (_hash_str(new_pass), user_id,)
                cursor.execute(sql, val)
                db.commit()
                _close(db)
                return True
            _close(db)
            return None
        _close(db)
        return None
    except mysql.connector.Error as err:
        print("Fehler beim Ändern des Passworts: " + err.msg)
        _close(db)
        return False


if __name__ == "__main__":
    db = _connect(_db_config_editor)
    _close(db)
